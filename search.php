
<?php

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

?>

<div class="search-page-wrapper">

	<div class="container">
				<?php if ( have_posts() && !(get_search_query() == '') ) { ?>
                    <header class="block-header">

							<h3 class="block-title">
								Search Results for: <span> <?php echo get_search_query(); ?> </span>
								
							</h3>

					</header><!-- .page-header -->
                    <div class="row">

					<?php /* Start the Loop */ ?>
					<?php
					while ( have_posts() ) {
						the_post();
                        $archive_post_img = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );

                        ?>

                            <div class="post-card">
                                <div class="post-card-image">
                                    <img src="<?php echo $archive_post_img; ?>" alt="">
                                </div>
                                <div class="post-card-body">
                                    <p class="cd-breadcrumbs"><?php foreach (get_categories() as $category){
                                echo $category->name;
                                echo "<span> / </span>";
                            } ?></p>
                                    <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                    <div class="meta-text">
                                        Published on <span class="meta-post-date"><?php echo get_the_date( 'j M Y' ); ?></span> by <span class="meta-post-author"><?php echo get_the_author_meta( 'display_name'); ?></span>
                                    </div>
                                </div>
                            </div> 

                        <?php
						
                        }
					?>

                        <!-- The pagination component -->
        <div class="archive-pagination">
            <?php
                    echo paginate_links( array(
                        'format' => '?paged=%#%',
                        'prev_text' => 'Previous Page',
                        'next_text' => 'Next Page'
                    ) );
            ?>
        </div>

                    
				<?php } else{ ?>
                    <div class="no-search-results">
                        <h4>Sorry! we don't have anything related to <span> <?php echo get_search_query(); ?> </span>.</h4>
                        <h5>Try searching something else.</h5>
                        <img src="/wp-content/uploads/2021/03/sign-post-5655110-svg-1.svg" alt="" class="">
                    </div>
                <?php } ?>
		</div><!-- .row -->

        

	</div><!-- #content -->

</div><!-- #search-wrapper -->

<?php
get_footer(); ?>


