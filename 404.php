<?php get_header(); ?>
    
    <div class="page-404-wrapper">
        <div class="container">
            <img src="/wp-content/uploads/2021/03/wind-5940755-svg.svg" alt="">
            <h3>Looks like you are lost!</h3>
            <h5>Lets start with the <a href="/">Home page</a> again...</h5>
        </div>
    </div>

<?php get_footer(); ?>