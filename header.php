<?php

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php do_action( 'wp_body_open' ); ?>
    <main>
        <nav class="site-navigation">
            <div class="container">
                <a rel="home" class="site-nav-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" itemprop="url">
                    <?php
                        $site_logo_id = get_theme_mod( 'custom_logo' );
                        $site_logo_img = wp_get_attachment_image_src( $site_logo_id , 'full' );
                        $site_logo_img_url = $site_logo_img[0];
                        $cd_site_title = get_bloginfo('name');

                        $logo_html = '<img src="'. $site_logo_img_url .'" class="custom-logo" alt="'. $cd_site_title .'">';
                        if(!has_custom_logo()){
                            echo $cd_site_title;
                        }else{
                            echo $logo_html;
                        }
                        
                    ?>
                </a>
                <div class="site-navigation-list-container">
                    <div class="site-navigation-list">
                        <ul>
                            <?php

                                if(!(is_front_page() || is_home())){
                                    echo '<li><a href="'. esc_url( home_url( '/' ) ) .'">Home</a></li>';
                                }
                        
                                wp_nav_menu( array(
                                    'container'         => "",
                                    'theme_location'    => "header_navigation",
                                    'items_wrap'        => '%3$s',
                                ) );
                            
                            ?>
                            <div class="follow-us">
                                <ul class="follow-us-list">
                                    <?php

                                        wp_nav_menu( array(
                                            'container'         => "",
                                            'theme_location'    => "social_menu",
                                            'items_wrap'        => '%3$s',
                                        ) );
                                    
                                    ?>
                                </ul>
                            </div>
                        </ul>
                        <p class="mobile-menu-close"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/cross-button.svg" alt=""></p>
                    </div>
                    <p class="mobile-menu-bars">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/radix-icons_hamburger-menu.svg" alt="">
                    </p>
                </div>
                <?php
                    $showHeaderSearch  = get_theme_mod( 'theme_menu_search_checkbox' );
                    if( $showHeaderSearch ){ ?>
                        <p class="cd-search cd-header-search">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/whh_magnifier.svg" alt="">
                        </p>
                    <?php
                    }   
                ?>
            </div>
        </nav>

        <header class="site-header">
            <div class="container">
                <!-- Page title starts here -->
                <?php
                    if(have_posts()){
                        while (have_posts()) { the_post();
                            if(get_field('show_page_title')){  echo '<h1 class="page-title">'. get_the_title() .'</h1>';}
                        }
                    }
                ?>
                <!-- Page title ends here -->
                <!-- Featured posts starts here -->
                <?php
                    if(get_field('show_featured_posts_section')){
                        ?>
                            <div class="featured-post-row row"> 
                                <?php 
                                    $first_two_posts = new WP_Query( array('posts_per_page' => 2,));
                                    if ( $first_two_posts->have_posts() ) :
                                        $count = 1; 
                                        while ( $first_two_posts->have_posts() ) : $first_two_posts->the_post(); 
                                            $featured_img_url = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) ); 
                                            if($count == 1){ ?>
                                                <div class="fp-left" style="background-image: url(<?php echo $featured_img_url; ?>)">
                                                    <p class="cd-breadcrumbs"><?php foreach (get_categories(array(
                                                'orderby' => 'name',
                                                'order'   => 'ASC',
                                            )) as $category){
                                echo $category->name;
                                echo "<span> / </span>";
                            } ?></p>
                                                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                                                    <p class="small-text"><?php echo excerpt(30,get_the_ID()); ?></p>
                                                    <a class="white-button" href="<?php the_permalink(); ?>">Read More</a>

                                                </div>
                                            <?php }
                                            elseif ($count == 2) { ?>
                                                <div class="fp-right">
                                                    <div class="post-card fp-card">
                                                        <div class="post-card-image">
                                                            <img src="<?php echo $featured_img_url; ?>" alt="">
                                                        </div>
                                                        <div class="post-card-body">
                                                            <p class="cd-breadcrumbs"><?php foreach (get_the_category(get_the_ID()) as $category){
                                echo $category->name;
                                echo "<span> / </span>";
                            } ?></p>
                                                            <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                                            <div class="meta-text">
                                                                Published on <span class="meta-post-date"><?php echo get_the_date( 'j M Y' ); ?></span> by <span class="meta-post-author"><?php echo get_the_author_meta( 'display_name'); ?></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php }
                                        $count++;
                                        endwhile;
                                        wp_reset_postdata();
                                    endif;
                                ?>
                            </div>
                        <?php
                    }
                ?>
                <!-- Featured posts ends here -->
                
            </div>           
        </header>

    
