//Toggle function for mobile menu sidebar
const menuHamburger = document.querySelector('.mobile-menu-bars');
const menuClose = document.querySelector('.mobile-menu-close');
const menuSidebar = document.querySelector('.site-navigation-list');
const searchIcon = document.querySelector('.cd-search');
const websiteSearch = document.querySelector('.website-search');
const followBtn = document.querySelector('.follow-us-button');
const followSocialList = document.querySelector('.follow-us-list');

menuHamburger.onclick = function(){
    menuSidebar.classList.add('active');
}

menuClose.onclick = function(){
    menuSidebar.classList.remove('active');
}

searchIcon.onclick = function(){
    if(this.classList.contains('search-visible')){
        this.classList.remove('search-visible');
        this.children[0].setAttribute('src','/wp-content/themes/codeta/assets/images/whh_magnifier.svg');
        websiteSearch.classList.remove('show-search');

    }else{
        this.classList.add('search-visible');
        this.children[0].setAttribute('src','/wp-content/themes/codeta/assets/images/cross-button.svg');
        websiteSearch.classList.add('show-search');
    }
}

jQuery(document).ready(function($){
  $('.posts-card-sldier').slick({
        slidesToShow: 3,
        arrows: true,
        infinite: false,
        responsive: [
            {
            breakpoint: 1200,
            settings: {
                slidesToShow: 2
            },
            
            },
            {
            breakpoint: 991,
            settings: {
                slidesToShow: 1
            },
            
            },
            
        ]
  });
});