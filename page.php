<?php get_header();  

if(have_posts()){
    while (have_posts()) {


?>

<?php
    if(get_field('show_latests_posts')){
        ?>
            <section class="latest-posts">
                <div class="container">
                    <?php
                        $latest_post_counts = get_field('latest_posts_count');
                        if($latest_post_counts < 4){
                            $latest_post_counts = 3;
                        }
                        $latest_posts_offset = 0;
                        if(get_field('show_featured_posts_section')){
                            $latest_posts_offset = 2;
                        }
                        $latest_post_query = new WP_Query(array('posts_per_page' => $latest_post_counts, 'offset' => $latest_posts_offset));
                        if($latest_post_query -> have_posts()){
                            while($latest_post_query -> have_posts()): $latest_post_query -> the_post();
                                $lp_img_url = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );
                            ?>
                                <div class="post-card">
                                    <div class="post-card-image">
                                        <img src="<?php echo $lp_img_url; ?>" alt="">
                                    </div>
                                    <div class="post-card-body">
                                        <p class="cd-breadcrumbs">
                                        <?php foreach (get_categories(array(
                                                'orderby' => 'name',
                                                'order'   => 'ASC',
                                            )) as $category){
                                                echo $category->name;
                                                echo "<span> / </span>";
                                            } 
                                        ?>
                                        </p>
                                        <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                        <div class="meta-text">
                                            Published on <span class="meta-post-date"><?php echo get_the_date( 'j M Y' ); ?></span> by <span class="meta-post-author"><?php echo get_the_author_meta( 'display_name'); ?></span>
                                        </div>
                                    </div>
                                </div>   
                            <?php
                            endwhile;
                            
                            wp_reset_postdata();
                        }   
                    ?>
                </div>            
            </section>
        <?php
    }
?>




<?php
    if(get_field('show__latest_news')){
        ?>

            <section class="latest-news bg-gray">
                <div class="container">
                    <h3>Latest News</h3>   
                    <div class="row">
                        <?php
                            $latest_news_counts = get_field('latests_news_count');
                            if($latest_news_counts < 3){
                                $latest_news_counts = 2;
                            }
                            $latest_news = new WP_Query(array('post_type'=> 'news', 'posts_per_page' => $latest_news_counts));
                            if($latest_news -> have_posts()):
                                while($latest_news -> have_posts()): $latest_news -> the_post();
                                    $ln_img_url = wp_get_attachment_image_url(get_post_thumbnail_id(get_the_ID()));
                                    ?>
                                        <div class="post-x-card">
                                            <div class="post-card-image">
                                                <img src="<?php echo $ln_img_url; ?>" alt="">
                                            </div>
                                            <div class="post-card-body">        
                                                <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                                <div class="meta-text">
                                                    <span class="meta-post-date"><?php echo get_the_date( 'j M Y' ); ?></span>
                                                </div>
                                            </div>
                                        </div>
                                    <?php
                                endwhile;
                                wp_reset_postdata();
                            endif;
                        ?>
                    </div>
                </div>            
            </section>

        <?php
    }
?>



<?php
    if(get_field('show_subscription_form')){

        $subscribe_form = get_field('subscription_form');

        ?>

            <section class="subscription-form">
                <div class="container">
                    <div class="sub-form-row row">
                        <div class="sub-form">
                            <h3 class="h3-big bold"><?php echo $subscribe_form['subscription_form_title']; ?></h3>
                            <p><?php echo $subscribe_form['subscription_form_description']; ?></p>
                        </div>
                        <div class="sub-form-image">
                            <img src="<?php echo esc_url( $subscribe_form['subscription_form_image']['url'] ); ?>" alt="">
                        </div>
                    </div>
                </div>
            </section>

        <?php
    }
?>



<?php
    if(get_field('show_trending_section')){
        ?>
            <section class="trending">
                <div class="container">
                    <h3 class="curve-underline">Trending</h3>
                    <?php  $popularpost = new WP_Query( array( 'post_type' => array('post','news'),'posts_per_page' => 10, 'meta_key' => 'codeta_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC'  ) ); ?>   
                    <div class="posts-card-sldier">
                        <?php
                        if($popularpost->have_posts()){
                            while($popularpost->have_posts()){
                                $popularpost->the_post();
                                $pp_img_url = wp_get_attachment_image_url(get_post_thumbnail_id(get_the_ID()));
                            ?>
                                <div class="post-card">
                                    <div class="post-card-image">
                                        <img src="<?php echo $pp_img_url; ?>" alt="<?php the_title(); ?>">
                                    </div>
                                    <div class="post-card-body">
                                        <p class="badge"><?php echo get_post_type(); ?></p>
                                        <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                        <div class="meta-text">
                                            Published on <span class="meta-post-date"><?php echo get_the_date( 'j M Y' ); ?></span>
                                        </div>
                                    </div>
                                </div>
                            <?php    
                            }
                            wp_reset_postdata();
                        }
                        ?>    
                    </div> 
                </div>
            </section>
        <?php
    }
?>



<?php
    if(get_field('show_featured_interview')){

        ?>

            <section class="interviews accent-bg">
                <div class="container">
                    <div class="title-bar">
                        <h3>Interviews</h3>
                        <a href="/interviews" class="white-button">View All Interviews</a>
                    </div>
                    <?php
                        $featured_interview = get_field('featured_interview');

                        if($featured_interview){
                                $interview_img_url = wp_get_attachment_image_url(get_post_thumbnail_id($featured_interview->ID));
                                ?>
                                    <div class="featured-interview-wrap row">
                                        <div class="featured-interview-img">
                                            <img src="<?php echo $interview_img_url; ?>" alt="<?php echo get_the_title($featured_interview->ID); ?>">
                                        </div>
                                        <div class="interview-content">
                                            <h4 class="big-h4"><a href="<?php echo get_the_permalink($featured_interview->ID); ?>"><?php echo get_the_title($featured_interview->ID); ?></a></h4>
                                            <p><?php echo get_field('current_position',$featured_interview->ID); ?></p>
                                            <p><?php echo get_field('current_company',$featured_interview->ID); ?></p>
                                            <p class="small-text"><?php echo excerpt(40,$featured_interview->ID); ?></p>
                                            <a href="<?php echo get_the_permalink($featured_interview->ID); ?>">Read More</a>
                                        </div>
                                    </div> 
                                <?php

                        }
                    ?>     
                </div>
            </section>

        <?php
        
    }
?>


<?php

        the_post();
        the_content();
    }
}
?>

<?php get_footer(); ?>



