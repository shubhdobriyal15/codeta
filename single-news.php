<?php get_header(); ?>

<div class="single-page-wrap">
    <div class="container single-post-container">
        <?php

            while (have_posts()) {
                the_post(); 
                $single_post_img = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );
                $current_post_type = get_post_type();
                ?>

                <div class="single-header">
                    <p class="cd-breadcrumbs"><?php foreach (get_categories() as $category){
                                echo $category->name;
                                echo "<span> / </span>";
                            } ?></p>
                    <h1 class="single-title">
                        <?php the_title(); ?>
                    </h1>
                    <div class="meta-text">
                        Published on <span class="meta-post-date"><?php echo get_the_date( 'j M Y' ); ?></span> by <span class="meta-post-author"><?php echo get_the_author_meta( 'display_name'); ?></span>
                    </div>
                </div>
                <div class="single-featured-image">
                    <img src="<?php echo $single_post_img; ?>" alt="<?php the_title(); ?>">
                </div>
                <div class="single-content">
                    <?php the_content(); ?>
                </div>
                <div class="single-social-share">

                </div>
            <?php    
            }

        ?>
        <div class="single-related-posts">
            <?php echo get_related_posts( get_the_ID(), 3,'news' ); ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>