'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
sass.compiler = require('node-sass');

var postcss = require('gulp-postcss');
var sourcemaps = require('gulp-sourcemaps');
var livereload = require('gulp-livereload');
const autoprefixer = require('autoprefixer');
var plumber = require('gulp-plumber');
const imagemin = require('gulp-imagemin');
var sassGlob = require('gulp-sass-glob');

const rootDir = './';
const targetDir = rootDir + 'dist/';
const sourceDir = rootDir + 'assets/';

var imageminOptions = [
  imagemin.gifsicle({interlaced: true}),
  imagemin.optipng({optimizationLevel: 5}),
  imagemin.svgo({
        plugins: [
            {removeViewBox: true},
            {cleanupIDs: false}
        ]
    }),
  imagemin.mozjpeg({progressive: true})
]
 
gulp.task('sass', function () {
  var plugins = [
        autoprefixer({cascade: false, grid: 'autoplace'})
    ];
  return gulp
            .src(sourceDir + 'scss/main.scss')
            .pipe(plumber())
            .pipe(postcss(plugins))
            .pipe(sourcemaps.init())
            .pipe(sassGlob())
            .pipe(sass.sync({outputStyle: 'compressed'}).on('error', sass.logError))
            .pipe(sourcemaps.write())
            .pipe(gulp.dest(targetDir + 'css'))
            .pipe(livereload());
});

gulp.task('images', function () {
     return gulp
              .src('../../uploads/**/*')
              .pipe(imagemin(imageminOptions))
              .pipe(gulp.dest('../../uploads'))
})
 
gulp.task('watch', function () {
  livereload.listen();
  gulp.watch(sourceDir + 'scss/**/*.scss', gulp.series('sass'));
  gulp.watch(sourceDir + 'images/**/*.{png,jpg,jpeg,gif,svg}', gulp.series('images'));
});

