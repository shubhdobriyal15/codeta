<?php

/*
Template Name: Archives
*/

?>
<?php 

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header(); ?>

<?php

    if(is_page('blog')){ $post_type = 'post'; }
    elseif (is_post_type_archive()) { $post_type = get_post_type(); }
    else{ $post_type = get_post_type(); }

    $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
    $archive_posts_query = new WP_Query(array('post_type' => $post_type, 'paged' => $paged,));

?>


<div class="archive-wrapper">
    <div class="archive-header">
        <div class="container">
            <h3 class="curve-underline">
                <?php if($post_type == 'post'){ echo 'Blogs'; }else{echo $post_type;}; ?>
            </h3>
        </div>
    </div>
    <div class="archive-posts-wrapper">
        <div class="container">
            <div class="row">
                <?php
                    $archive_posts_count = 1;
                    if($archive_posts_query->have_posts()){
                        while ($archive_posts_query->have_posts()) {
                        
                            $archive_posts_query->the_post();

                            $archive_post_img = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );

                            if($archive_posts_count == 7){ ?>

                                <section class="subscription-form">
                                    <div class="container">
                                        <div class="sub-form-row row">
                                            <div class="sub-form">
                                                <h3 class="h3-big bold">Subscribe to our newsletter</h3>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ornare et morbi augue non ullamcorper consectetur. Massa adipiscing dolor ut amet, mauris</p>
                                            </div>
                                        </div>
                                    </div>
                                </section>

                            <?php }

                            ?>

                                <div class="post-card">
                                    <div class="post-card-image">
                                        <img src="<?php echo $archive_post_img; ?>" alt="">
                                    </div>
                                    <div class="post-card-body">
                                        <p class="cd-breadcrumbs"><?php foreach (get_the_category() as $category){
                                echo $category->name;
                                echo "<span> / </span>";
                            } ?></p>
                                        <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                        <div class="meta-text">
                                            Published on <span class="meta-post-date"><?php echo get_the_date( 'j M Y' ); ?></span> by <span class="meta-post-author"><?php echo get_the_author_meta( 'display_name'); ?></span>
                                        </div>
                                    </div>
                                </div> 

                            <?php
                            $archive_posts_count++;
                        }

                        
                    }


                
                ?>
            </div>
            <div class="archive-pagination">
                <?php
                    $big = 999999999;
                        echo paginate_links( array(
                            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                            'format' => '?paged=%#%',
                            'current' => max( 1, get_query_var('paged') ),
                            'total' => $archive_posts_query->max_num_pages,
                            'prev_text' => 'Previous Page',
                            'next_text' => 'Next Page'
                        ) );
                ?>
            </div>
        </div>
    </div>
</div>




<?php get_footer(); ?>