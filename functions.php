<?php
// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

// Includes directory.
$theme_inc_dir = get_template_directory() . '/inc';

// Array of files to include.
$understrap_includes = array(
	'/setup.php',                           // Theme setup and custom theme supports.
	'/widgets.php',                         // Register widget area.
	'/enqueue.php',                         // Enqueue scripts and styles.
	'/pagination.php',                      // Custom pagination for this theme.
	'/hooks.php',                           // Custom hooks.
	'/extras.php',                          // Custom functions that act independently of the theme templates.
	'/custom-comments.php',                 // Custom Comments file.
	'/customizer.php',	// theme customizer
	'/cpt.php', //Custom post types file
	'/shortcodes.php' //shortcodes file
);

// Include files.
foreach ( $understrap_includes as $file ) {
	require_once $theme_inc_dir . $file;
}


function get_related_posts( $post_id, $related_count,$post_type='post', $args = array() ) {
  $terms = get_the_terms( $post_id, 'category' );
  
  if ( empty( $terms ) ) $terms = array();
  
  $term_list = wp_list_pluck( $terms, 'slug' );

  if($post_type == 'news'){
	  $related_args = array(
		'post_type' => $post_type,
		'posts_per_page' => $related_count,
		'post_status' => 'publish',
		'post__not_in' => array( $post_id ),
		'orderby' => 'rand'
	);
  }else{
	  $related_args = array(
		'post_type' => $post_type,
		'posts_per_page' => $related_count,
		'post_status' => 'publish',
		'post__not_in' => array( $post_id ),
		'orderby' => 'rand',
		'tax_query' => array(
			array(
				'taxonomy' => 'category',
				'field' => 'slug',
				'terms' => $term_list
			)
		)
	);
  }
  
  $related_args = array(
    'post_type' => $post_type,
    'posts_per_page' => $related_count,
    'post_status' => 'publish',
    'post__not_in' => array( $post_id ),
    'orderby' => 'rand',
    'tax_query' => array(
      $tax_query_args
    )
  );

  $the_query = new WP_Query( $related_args );

  	global $cl_from_element;
	$cl_from_element['is_related'] = true;	
					
	// Display posts
	if ( $the_query->have_posts() ){
		$relatedPostsHtml = '<section class="related-posts"><h3>Related Posts</h3> ';	

		while ( $the_query->have_posts() ) : $the_query->the_post();
			$rp_img_url = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );

			$relatedPostsHtml .= '<div class="post-card">
									<img src="'.$rp_img_url.'" alt="">
                                    <div class="post-card-body">
                                        <h4><a href="'.get_the_permalink().'">'.get_the_title().'</a></h4>
                                        <div class="meta-text">
                                            Published on <span class="meta-post-date">'.get_the_date( 'j M Y' ).'</span> by <span class="meta-post-author">'.get_the_author_meta( 'display_name').'</span>
                                        </div>
                                    </div>
                                </div>  ';
        endwhile;
	}
	wp_reset_query();
	$relatedPostsHtml .= '</section>';	
	$cl_from_element['is_related'] = false;
	return $relatedPostsHtml;
}

function initCors( $value ) {
  $origin = get_http_origin();
  $allowed_origins = [ 'shubh-portfolio.vercel.app' ];

  if ( $origin && in_array( $origin, $allowed_origins ) ) {
    header( 'Access-Control-Allow-Origin: ' . esc_url_raw( $origin ) );
    header( 'Access-Control-Allow-Methods: GET' );
    header( 'Access-Control-Allow-Credentials: true' );
  }

  return $value;
}

// ... initCors function

add_action( 'rest_api_init', function() {

	remove_filter( 'rest_pre_serve_request', 'rest_send_cors_headers' );

	add_filter( 'rest_pre_serve_request', initCors);
}, 15 );