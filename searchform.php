<?php
defined( 'ABSPATH' ) || exit;
?>

<div class="container">
	<form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
		<label class="sr-only" for="s"><?php esc_html_e( 'Search', 'codeta' ); ?></label>
		<div class="input-group">
			<input class="field form-control" id="s" name="s" type="text"
				placeholder="<?php esc_attr_e( 'Search something like "how to &hellip;"', 'codeta' ); ?>" value="<?php the_search_query(); ?>">
			<button type="submit"><img src="/wp-content/uploads/2021/03/white-magnifier.svg" alt=""></button>
		</div>
	</form>
</div>
