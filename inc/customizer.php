<?php
/**
 * Theme Customizer
 *
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
if ( ! function_exists( 'codeta_customize_register' ) ) {
	/**
	 * Register basic customizer support.
	 *
	 * @param object $wp_customize Customizer reference.
	 */
	function codeta_customize_register( $wp_customize ) {
		$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
		$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
		$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
	}
}
add_action( 'customize_register', 'codeta_customize_register' );

if ( ! function_exists( 'codeta_theme_customize_register' ) ) {
	/**
	 * Register individual settings through customizer's API.
	 *
	 * @param WP_Customize_Manager $wp_customize Customizer reference.
	 */
	function codeta_theme_customize_register( $wp_customize ) {

		// Theme layout settings.
		$wp_customize->add_section(
			'codeta_theme_layout_options',
			array(
				'title'       => __( 'Theme Layout Settings', 'codeta' ),
				'capability'  => 'edit_theme_options',
				'description' => __( 'Container width and sidebar defaults', 'codeta' ),
				'priority'    => apply_filters( 'codeta_theme_layout_options_priority', 160 ),
			)
		);

		//Option to show or hide the search in menu
		$wp_customize->add_section( 'theme_search_option', array(
			'title' => 'Theme Search',
			'priority' => 10,
			'description' => 'Options for search in website'
		));
 
		/**
		 * Select sanitization function
		 *
		 * @param string               $input   Slug to sanitize.
		 * @param WP_Customize_Setting $setting Setting instance.
		 * @return string Sanitized slug if it is a valid choice; otherwise, the setting default.
		 */
		function codeta_theme_slug_sanitize_select( $input, $setting ) {

			// Ensure input is a slug (lowercase alphanumeric characters, dashes and underscores are allowed only).
			$input = sanitize_key( $input );

			// Get the list of possible select options.
			$choices = $setting->manager->get_control( $setting->id )->choices;

			// If the input is a valid key, return it; otherwise, return the default.
			return ( array_key_exists( $input, $choices ) ? $input : $setting->default );

		}

		$wp_customize->add_setting(
			'codeta_container_type',
			array(
				'default'           => 'container',
				'type'              => 'theme_mod',
				'sanitize_callback' => 'codeta_theme_slug_sanitize_select',
				'capability'        => 'edit_theme_options',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'codeta_container_type',
				array(
					'label'       => __( 'Container Width', 'codeta' ),
					'description' => __( 'Choose between Bootstrap\'s container and container-fluid', 'codeta' ),
					'section'     => 'codeta_theme_layout_options',
					'settings'    => 'codeta_container_type',
					'type'        => 'select',
					'choices'     => array(
						'container'       => __( 'Fixed width container', 'codeta' ),
						'container-fluid' => __( 'Full width container', 'codeta' ),
					),
					'priority'    => apply_filters( 'codeta_container_type_priority', 10 ),
				)
			)
		);

		$wp_customize->add_setting(
			'codeta_sidebar_position',
			array(
				'default'           => 'right',
				'type'              => 'theme_mod',
				'sanitize_callback' => 'sanitize_text_field',
				'capability'        => 'edit_theme_options',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'codeta_sidebar_position',
				array(
					'label'             => __( 'Sidebar Positioning', 'codeta' ),
					'description'       => __(
						'Set sidebar\'s default position. Can either be: right, left, both or none. Note: this can be overridden on individual pages.',
						'codeta'
					),
					'section'           => 'codeta_theme_layout_options',
					'settings'          => 'codeta_sidebar_position',
					'type'              => 'select',
					'sanitize_callback' => 'codeta_theme_slug_sanitize_select',
					'choices'           => array(
						'right' => __( 'Right sidebar', 'codeta' ),
						'left'  => __( 'Left sidebar', 'codeta' ),
						'both'  => __( 'Left & Right sidebars', 'codeta' ),
						'none'  => __( 'No sidebar', 'codeta' ),
					),
					'priority'          => apply_filters( 'codeta_sidebar_position_priority', 20 ),
				)
			)
		);


		$wp_customize->add_setting( 'theme_menu_search_checkbox', array(
				'default' => '',
		));
	
		$wp_customize->add_control( 'theme_menu_search_control', array(
					'label' => 'Enable search in main header',
					'type'  => 'checkbox', // this indicates the type of control
					'section' => 'theme_search_option',
					'settings' => 'theme_menu_search_checkbox'
		));


		$wp_customize->add_setting( 'cd-accent-color', array(
				'default' => '#0078E7',
		));

		$wp_customize->add_control( 'cd-accent-color-control', array(
					'label' => 'Accent Color',
					'type'  => 'color',
					'section' => 'colors',
					'settings' => 'cd-accent-color'
		));

	

	}
} // End of if function_exists( 'codeta_theme_customize_register' ).
add_action( 'customize_register', 'codeta_theme_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
if ( ! function_exists( 'codeta_customize_preview_js' ) ) {
	/**
	 * Setup JS integration for live previewing.
	 */
	function codeta_customize_preview_js() {
		wp_enqueue_script(
			'codeta_customizer',
			get_template_directory_uri() . '/js/customizer.js',
			array( 'customize-preview' ),
			'20130508',
			true
		);
	}
}
add_action( 'customize_preview_init', 'codeta_customize_preview_js' );
