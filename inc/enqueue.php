<?php
// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

if ( ! function_exists( 'codeta_scripts' ) ) {
	/**
	 * Load theme's JavaScript and CSS sources.
	 */
	function codeta_scripts() {
		// Get the theme data.
		$the_theme     = wp_get_theme();
		$theme_version = $the_theme->get( 'Version' );

		$css_version = $theme_version . '.' . filemtime( get_template_directory() . '/dist/css/main.css' );
		wp_enqueue_style( 'codeta-styles', get_template_directory_uri() . '/dist/css/main.css', array(), $css_version );

		wp_enqueue_style( 'slick-carousel-css', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css', array(),'1.9.0' );
		wp_enqueue_style( 'slick-carousel-theme-css', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css', array(),'1.9.0' );

		wp_enqueue_style( 'Google fonts', 'https://fonts.googleapis.com/css2?family=Lato&family=Merriweather:wght@400;700;900&display=swap', array(),null);

		

		wp_enqueue_script( 'jquery' );

		$js_version = $theme_version . '.' . filemtime( get_template_directory() . '/assets/js/main.js' );
		wp_enqueue_script( 'codeta-scripts', get_template_directory_uri() . '/assets/js/main.js', array(), $js_version, true );

		wp_enqueue_script( 'fontawesome-scripts', get_template_directory_uri() . '/assets/js/vendor/fontawesome.min.js', array(), '5.15.2', true );

		wp_enqueue_script( 'slick-carousel','https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js', array('jquery'), '1.9.0', true );

		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}
} // End of if function_exists( 'codeta_scripts' ).

add_action( 'wp_enqueue_scripts', 'codeta_scripts' );
