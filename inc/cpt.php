<?php
/*
* This file contains all the custom post types used in the website
*
*/


function custom_post_type() {

    $labels = array(
        'name'                => _x( 'News', 'Post Type General Name', 'codeta' ),
        'singular_name'       => _x( 'News', 'Post Type Singular Name', 'codeta' ),
        'menu_name'           => __( 'News', 'codeta' ),
        'parent_item_colon'   => __( 'Parent News', 'codeta' ),
        'all_items'           => __( 'All News', 'codeta' ),
        'view_item'           => __( 'View News', 'codeta' ),
        'add_new_item'        => __( 'Add New News', 'codeta' ),
        'add_new'             => __( 'Add New', 'codeta' ),
        'edit_item'           => __( 'Edit News', 'codeta' ),
        'update_item'         => __( 'Update News', 'codeta' ),
        'search_items'        => __( 'Search News', 'codeta' ),
        'not_found'           => __( 'Not Found', 'codeta' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'codeta' ),
    );

    $args = array(
        'label'               => __( 'news', 'codeta' ),
        'description'         => __( 'Latest tech news and updates', 'codeta' ),
        'labels'              => $labels,
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'show_in_rest' => true,
        'menu_icon' => 'dashicons-welcome-widgets-menus'

    );

    // Registering your Custom Post Type
    register_post_type( 'news', $args );

    $interview_labels = array(
        'name'                => _x( 'Interviews', 'Post Type General Name', 'codeta' ),
        'singular_name'       => _x( 'Interview', 'Post Type Singular Name', 'codeta' ),
        'menu_name'           => __( 'Interviews', 'codeta' ),
        'parent_item_colon'   => __( 'Parent Interviews', 'codeta' ),
        'all_items'           => __( 'All Interviews', 'codeta' ),
        'view_item'           => __( 'View Interview', 'codeta' ),
        'add_new_item'        => __( 'Add New Interview', 'codeta' ),
        'add_new'             => __( 'Add New', 'codeta' ),
        'edit_item'           => __( 'Edit Interview', 'codeta' ),
        'update_item'         => __( 'Update Interview', 'codeta' ),
        'search_items'        => __( 'Search Interview', 'codeta' ),
        'not_found'           => __( 'Not Found', 'codeta' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'codeta' ),
    );

    $interview_args = array(
        'label'               => __( 'interviews', 'codeta' ),
        'description'         => __( 'Interviews of professionals', 'codeta' ),
        'labels'              => $interview_labels,
        'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'show_in_rest' => true,
        'menu_icon' => 'dashicons-admin-users'

    );

    // Registering your Custom Post Type
    register_post_type( 'interviews', $interview_args );

}
add_action( 'init', 'custom_post_type', 0 );


?>