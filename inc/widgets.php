<?php
// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;


add_action( 'widgets_init', 'codeta_widgets_init' );

if ( ! function_exists( 'codeta_widgets_init' ) ) {
	/**
	 * Initializes themes widgets.
	 */
	function codeta_widgets_init() {
		register_sidebar(
			array(
				'name'          => __( 'Search Widget', 'codeta' ),
				'id'            => 'cd-search-widget',
				'description'   => __( 'Search widget for website', 'codeta' ),
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			)
		);
	}
} // End of function_exists( 'codeta_widgets_init' ).
