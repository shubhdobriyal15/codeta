
    <footer>
        <div class="container">
            <p>&copy; <?php echo date('Y');  bloginfo('name');  ?></p>
        </div>
    </footer>

</main>

<div class="website-search">
    <?php get_search_form(); ?>
</div>

<?php wp_footer(); ?>

</body>
</html>